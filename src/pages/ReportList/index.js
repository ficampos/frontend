import React, { useEffect, useState } from "react";
import api from "../../services/api";

import "./styles.css";

function ReportList({ history, match }) {
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [status, setStatus] = useState("");
  const [category, setCategory] = useState("");
  const [categorys, setCategorys] = useState([]);
  const [project, setProject] = useState("");
  const [user, setUser] = useState("");
  const [message, setMessage] = useState("");

  const { id } = match.params;
  const { reportid } = match.params;

  useEffect(() => {
    async function loadReport() {
      const response = await api.get(`/reports/${reportid}`);

      if (response.status === 200) {
        setTitle(response.data.title);
        setDescription(response.data.description);
        setStatus(response.data.status);
        setCategory(response.data.category);
        setProject(response.data.project);
        setUser(response.data.user);
      } else if (response.status === 404) {
        alert("error");
      }
    }
    loadReport();
  }, [reportid]);

  useEffect(() => {
    async function loadCategorys() {
      const response = await api.get(`/categorys/project/${id}`);

      if (response.status === 200) {
        setCategorys(response.data);
      } else if (response.status === 404) {
        alert("error");
      }
    }
    loadCategorys();
  }, [id]);

  async function handleEdit(e) {
    e.preventDefault();
    const response = await api.put(`/reports/${reportid}`, {
      title,
      description,
      status,
      project,
      category: { id: parseInt(category.id) },
      user,
    });
    console.log(response);
    if (response.status === 204) {
      history.push(`/project/${id}`);
    } else if (response.status === 400) {
      setMessage(response.message);
    }
  }
  return (
    <>
      <div id="ReportList">
        <div className="ReportList">
          <form onSubmit={handleEdit}>
            <div className="input-block-edit">
              <label htmlFor="title">Titulo do report</label>
              <input
                name="title"
                id="title"
                required
                type="text"
                value={title}
                onChange={(e) => setTitle(e.target.value)}
              />
            </div>
            <div className="input-block-edit">
              <label htmlFor="description">Descrição</label>
              <input
                name="description"
                id="description"
                required
                type="text"
                value={description}
                onChange={(e) => setDescription(e.target.value)}
              />
            </div>

            <div className="input-group">
              <div className="input-block-edit">
                <label htmlFor="status">Status</label>
                <select
                  required
                  value={status}
                  onChange={(e) => setStatus(e.target.value)}
                >
                  <option value=""></option>
                  <option value="Aberto">Aberto</option>
                  <option value="Avaliacao">Avaliação</option>
                  <option value="Concluido">Concluído</option>
                  <option value="Em Progresso">Em Progresso</option>
                </select>
              </div>
              <div className="input-block-edit">
                <label htmlFor="category">Categoria</label>
                <select
                  required
                  value={category.id}
                  onChange={(e) => setCategory(e.target.value)}
                >
                  <option value="">Selecione uma categoria</option>
                  {categorys.map((c) => (
                    <option key={c.id} value={c.id}>
                      {c.name}
                    </option>
                  ))}
                </select>
              </div>
            </div>
            <hr />
            <div className="button-group">
              <button type="submit">Editar</button>
              <button
                onClick={() => {
                  history.goBack();
                }}
              >
                Voltar
              </button>
            </div>
          </form>
        </div>
      </div>
    </>
  );
}

export default ReportList;
