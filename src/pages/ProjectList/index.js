import React, { useEffect, useState } from "react";
import api from "../../services/api";
import { Link } from "react-router-dom";

import PageHeader from "../../components/PageHeader";
import Delete from "../../assets/images/delete.png";
import Editar from "../../assets/images/editar.png";

import "../../styles/global.css";
import "./styles.css";

import Modal from "../../components/Modal";

function ProjectList({ history }) {
  const [projects, setProjects] = useState([]);
  const [modalVisible, setModalVisible] = useState(false);
  const [modalBody, setModalBody] = useState("");
  const [projectToDelete, setProjectToDelete] = useState(null);

  useEffect(() => {
    async function loadProjects() {
      const response = await api.get("projects/myprojects");
      setProjects(response.data);
    }
    loadProjects();
  }, []);

  const handleDelete = (e, project) => {
    e.stopPropagation();
    setModalBody(`Tem certeza que deseja remover o projeto ${project.name}?`);
    setProjectToDelete(project);
    setModalVisible(true);
  };

  const handleEdit = (e, projectId) => {
    e.stopPropagation();
    history.push(`/project/${projectId}/edit`);
  };

  async function handleConfirm(data) {
    const { id } = data;
    const response = await api.delete(`projects/${id}`);
    if (response.status === 204) {
      const projectsNew = projects.filter((p) => p.id !== id);
      setProjects(projectsNew);
    } else if (response.status === 400) {
      console.log("erro");
    }
  }

  const showProject = (projectId) => {
    // navega pra pagina de detalhes
    history.push(`/project/${projectId}`);
  };

  return (
    <>
      <PageHeader />
      <Modal
        title="Confirmação de exclusão"
        visible={modalVisible}
        setVisible={setModalVisible}
        body={modalBody}
        data={projectToDelete}
        onConfirm={handleConfirm}
      />

      <div className="container-center project-list">
        <div className="owner-project-list">
          <div className="title-container">
            <strong>Meus Projetos</strong>
            <Link to="/project/new" className="create-project-button">
              + Novo Projeto
            </Link>
          </div>
          <main className="project-list--cards-container">
            {projects.map((project) => (
              <article
                key={project.id}
                className="project-list--card"
                onClick={() => showProject(project.id)}
              >
                <header>
                  <h3>{project.name}</h3>
                  <span>{project.visibility ? "Público" : "Prívado"}</span>
                </header>
                <footer>
                  <div>{project.createdAt}</div>
                  <div>
                    <img
                      className="project-list--card-buttons"
                      src={Editar}
                      onClick={(e) => handleEdit(e, project.id)}
                      alt="Botão de editar"
                    />
                    <img
                      className="project-list--card-buttons"
                      src={Delete}
                      onClick={(e) => handleDelete(e, project)}
                      alt="Botão de deletar"
                    />
                  </div>
                </footer>
              </article>
            ))}
          </main>
        </div>
        <div className="owner-project-list">
          <div className="title-container">
            <strong>Projetos Públicos</strong>
          </div>
          <main className="project-list--cards-container"></main>
        </div>
      </div>
    </>
  );
}

export default ProjectList;
