import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

import api from "../../services/api";
import Delete from "../../assets/images/delete.png";
import Editar from "../../assets/images/editar.png";
import PageHeader from "../../components/PageHeader";
import Modal from "../../components/Modal";

import "./styles.css";

function Project({ history, match }) {
  const [project, setProject] = useState("");
  const [users, setUsers] = useState([]);
  const [reports, setReports] = useState([]);
  const [modalVisible, setModalVisible] = useState(false);
  const [modalBody, setModalBody] = useState("");
  const [reportToDelete, setreportToDelete] = useState(null);
  const { id } = match.params;

  useEffect(() => {
    async function loadProject() {
      const response = await api.get(`/projects/${id}`);

      if (response.status === 200) {
        setProject(response.data);
      } else if (response.status === 404) {
        alert("error");
      }
    }
    loadProject();
  }, [id]);

  useEffect(() => {
    async function loadUsersProject() {
      const response = await api.get(`/projects/${id}/participants`);

      if (response.status === 200) {
        setUsers(response.data);
      } else if (response.status === 404) {
        alert("error");
      }
    }
    loadUsersProject();
  }, [id]);

  useEffect(() => {
    async function loadProjectReports() {
      const response = await api.get(`/projects/${id}/reports`);

      if (response.status === 200) {
        setReports(response.data);
      } else if (response.status === 404) {
        alert("error");
      }
    }
    loadProjectReports();
  }, [id]);

  const handleDelete = (e, report) => {
    e.stopPropagation();
    setModalBody(`Tem certeza que deseja remover o report ${report.title}?`);
    setreportToDelete(report);
    setModalVisible(true);
  };

  const showReport = (reportId) => {
    // navega pra pagina de detalhes
    history.push(`/project/${id}/report/${reportId}`);
  };

  const handleEdit = (e, reportId) => {
    e.stopPropagation();
    history.push(`/project/${id}/report/${reportId}/edit`);
  };

  async function handleConfirm(data) {
    const { id } = data;
    const response = await api.delete(`reports/${id}`);
    if (response.status === 204) {
      const reportsNew = reports.filter((r) => r.id !== id);
      setReports(reportsNew);
    } else if (response.status === 400) {
      console.log("erro");
    }
  }

  return (
    <>
      <PageHeader />
      <Modal
        title="Confirmação de exclusão"
        visible={modalVisible}
        setVisible={setModalVisible}
        body={modalBody}
        data={reportToDelete}
        onConfirm={handleConfirm}
      />
      <div className="container-center project">
        <div className="title-container">
          <div className="project-info">
            <strong>{project.name}</strong>
            <div className="metadata">
              <span className="project-visibility">
                {project.visibility ? "Público" : "Prívado"}
              </span>
              <span className="project-dt-create">{project.createdAt}</span>
            </div>
          </div>
          <Link
            to={`/project/${project.id}/report/new`}
            className="create-report-button"
          >
            + Novo Report
          </Link>
        </div>
        <main className="project--container">
          <div className="project-description-container">
            <div className="project-description">
              <p>{project.description}</p>
            </div>

            <div className="project-users">
              {users.map((user) => (
                <span key={user.id}>{user.firstName && user.firstName[0]}</span>
              ))}
            </div>
          </div>
          <div className="project--report-cards">
            {reports.map((report) => (
              <article
                key={report.id}
                className="project--report-card"
                onClick={() => showReport(report.id)}
              >
                <header>
                  <h3>{report.title}</h3>
                  <span>{report.statusDescription}</span>
                </header>
                <footer>
                  <div>{report.createdAt}</div>
                </footer>
              </article>
            ))}
          </div>
        </main>
      </div>
    </>
  );
}

export default Project;
