export const StatusReportEnum = [
  {
    value: 0,
    label: "Aberto",
  },
  {
    value: 1,
    label: "Avaliação",
  },
  {
    value: 2,
    label: "Concluído",
  },
  {
    value: 3,
    label: "Em Progresso",
  },
  {
    value: 4,
    label: "Arquivado",
  },
];
