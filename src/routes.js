import React from "react";
import {
  BrowserRouter,
  Route,
  Router,
  Switch,
  Redirect,
} from "react-router-dom";

import { isAuthenticated } from "./services/auth";
import { history } from "./helpers/history";

import User from "./pages/User";
import Login from "./pages/Login";
import Register from "./pages/Register";
import UserForm from "./pages/UserForm";

import Project from "./pages/Project";
import ProjectList from "./pages/ProjectList";
import ProjectForm from "./pages/ProjectForm";

import Report from "./pages/Report";
import ReportList from "./pages/ReportList";
import ReportForm from "./pages/ReportForm";

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={(props) =>
      isAuthenticated() ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{ pathname: "/login", state: { from: props.location } }}
        />
      )
    }
  />
);

const OnlyPublicRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={(props) =>
      !isAuthenticated() ? (
        <Component {...props} />
      ) : (
        <Redirect to={{ pathname: "/" }} />
      )
    }
  />
);

const Routes = () => (
  <BrowserRouter>
    <Router history={history}>
      <Switch>
        <OnlyPublicRoute path="/login" component={Login} />
        <OnlyPublicRoute path="/cadastro" component={Register} />
        <PrivateRoute exact path="/user/:nickname" component={User} />
        <PrivateRoute exact path="/user/:id/edit" component={UserForm} />
        <PrivateRoute exact path="/" component={ProjectList} />
        <PrivateRoute path="/project/new" component={ProjectForm} />
        <PrivateRoute path="/project/:id/edit" component={ProjectForm} />
        <PrivateRoute exact path="/project/:id" component={Project} />
        <PrivateRoute path="/report" component={ReportList} />
        <PrivateRoute path="/project/:id/report/new" component={ReportForm} />
        <PrivateRoute
          exact
          path="/project/:id/report/:reportid/edit"
          component={ReportForm}
        />
        <PrivateRoute path="/project/:id/report/:reportid" component={Report} />

        <Route path="*" component={() => <h1>Pagina não encontrada</h1>} />
      </Switch>
    </Router>
  </BrowserRouter>
);

export default Routes;
