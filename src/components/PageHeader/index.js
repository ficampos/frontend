import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

import { Button } from "../../components/Button";
import logo from "../../assets/images/Logo.png";
import { isAuthenticated, getUserData, logout } from "../../services/auth";

import "./styles.css";

function PageHeader() {
  const [loggedUser, setLoggedUser] = useState(null);

  useEffect(() => {
    if (isAuthenticated()) {
      const { user } = getUserData();
      setLoggedUser(user);
    }
  }, []);

  return (
    <header className="page-header">
      <div className="container-center top-bar-container">
        <img src={logo} alt="FidBack Logo" />
        <div
          className={
            !isAuthenticated()
              ? `navigation-bar`
              : `navigation-bar-isAuthenticated`
          }
        >
          {!isAuthenticated() ? (
            <>
              <Link className="create-account-button" to="/cadastro">
                Criar uma conta
              </Link>
              <Link className="login-button" to="/">
                Entrar
              </Link>
            </>
          ) : (
            <>
              <div className="menu-navigation">
                <div className="page-menu-navigation">
                  <Button menu="true" to="/">
                    Projetos
                  </Button>
                  <Button menu="true" to="/">
                    Reports
                  </Button>
                  <Button menu="true" to="/">
                    Sobre
                  </Button>
                </div>
                <div className="user-menu-navigation">
                  <Button
                    menu="true"
                    to={`/user/${loggedUser && loggedUser.nickname}`}
                  >
                    {loggedUser && loggedUser.nickname}
                  </Button>
                  <Button
                    menu="true"
                    to="/"
                    onClick={() => {
                      logout();
                    }}
                  >
                    Sair
                  </Button>
                </div>
              </div>
            </>
          )}
        </div>
      </div>
    </header>
  );
}

export default PageHeader;
